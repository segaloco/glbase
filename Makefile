CFLAGS_U	=	-g
LIBS_U		=	-lX11 -lGL

CFLAGS_W	=	/Zi
LDFLAGS_W	=	/debug /subsystem:windows /entry:mainCRTStartup
LIBS_W		=	user32.lib gdi32.lib opengl32.lib

BIN		=	game
BIN_W		=	$(BIN).exe
ILK_W		=	$(BIN).ilk
PDB_W		=	$(BIN).pdb
VCPDB_W		=	vc*.pdb

OBJS		=	main.o \
			strings.o \
			sys_u.o

OBJS_W		=	main.obj \
			strings.obj \
			sys_w.obj

all: $(BIN)

clean:	clean_u

$(BIN): $(OBJS)
	$(CC) -o $(BIN) $(OBJS) $(LIBS_U)

$(BIN_W): $(OBJS_W)
	link $(LDFLAGS_W) -out:$(BIN_W) $** $(LIBS_W)

.c.o:
	$(CC) $(CFLAGS_U) -c -o $@ $<

.c.obj:
	cl $(CFLAGS_W) /c $*.c

clean_u:
	rm -rf $(BIN) $(OBJS)

clean_w:
	del /f $(BIN_W) $(ILK_W) $(PDB_W) $(VCPDB_W) $(OBJS_W)