/*
 * Copyright 2024 Matthew Gilmore
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <stddef.h>
#include <stdlib.h>

#include <Windows.h>

#include "sys.h"

static const LPCTSTR gfxWndClass = TEXT("gfxWndClass");
static const LPCTSTR gfxWndName = TEXT("gfxWnd");
static const PIXELFORMATDESCRIPTOR pfd = {
	sizeof (PIXELFORMATDESCRIPTOR),
	1,
	PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER,
	PFD_TYPE_RGBA,
	32,
	0, 0, 0, 0, 0, 0,
	0,
	0,
	0,
	0, 0, 0, 0,
	24,
	8,
	0,
	PFD_MAIN_PLANE,
	0,
	0, 0, 0
};

static HINSTANCE hInstance = NULL;
static HWND hWnd = NULL;
static HDC hDC = NULL;
static HGLRC hGLRC = NULL;

static LRESULT WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

static void UnregisterWndClass(void);
static void DestroyWnd(void);

int sys_init(const sys_config_t *config)
{
	WNDCLASS wndClass = { 0 };

	if ((hInstance = GetModuleHandle(NULL)) == NULL)
		return -1;

	wndClass.style		= CS_OWNDC;
	wndClass.lpfnWndProc	= WndProc;
	wndClass.hInstance	= hInstance;
	wndClass.hIcon		= NULL;
	wndClass.hCursor	= NULL;
	wndClass.hbrBackground	= (HBRUSH) (COLOR_BACKGROUND);
	wndClass.lpszMenuName	= NULL;
	wndClass.lpszClassName	= gfxWndClass;

	if (!RegisterClass(&wndClass))
		return -1;
	else
		atexit(UnregisterWndClass);

	if ((hWnd = CreateWindow(
		wndClass.lpszClassName,
		gfxWndName,
		WS_OVERLAPPEDWINDOW|WS_VISIBLE,
		config->posx, config->posy,
		config->width, config->height,
		NULL,
		NULL,
		hInstance,
		NULL
	)) == NULL)
		return -1;
	else
		atexit(DestroyWnd);

	return 0;
}

void sys_sync(void)
{
	MSG msg;

	while (PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE)) {
		if (msg.message == WM_QUIT)
			exit(msg.wParam);
		else
			DispatchMessage(&msg);
	}

	SwapBuffers(hDC);
}

static void UnregisterWndClass(void)
{
	UnregisterClass(gfxWndClass, hInstance);
}

static void DestroyWnd(void)
{
	if (hWnd != NULL)
		DestroyWindow(hWnd);

	hWnd = NULL;
}

static LRESULT WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
	case WM_CREATE:
		if ((hDC = GetDC(hWnd)) == NULL)
			return -1;

		if (SetPixelFormat(
			hDC,
			ChoosePixelFormat(hDC, &pfd),
			&pfd
		) == FALSE)
			return -1;

		if ((hGLRC = wglCreateContext(hDC)) == NULL)
			return -1;

		wglMakeCurrent(hDC, hGLRC);
		
		break;
	case WM_CLOSE:
		DestroyWnd();
		break;
	case WM_DESTROY:
		if (hGLRC != NULL)
			wglDeleteContext(hGLRC);

		if (hDC != NULL)
			ReleaseDC(hWnd, hDC);

		PostQuitMessage(EXIT_SUCCESS);
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	return 0;
}