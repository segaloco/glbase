/*
 * Copyright 2024 Matthew Gilmore
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <stddef.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <GL/glx.h>

#include "sys.h"

static Display *dpy = NULL;
static Window win;
static Atom wm_delete_window;

static GLXContext ctx;
static GLXWindow gwin;

static void event_proc(const XEvent *event);

static void close_display(void);
static void destroy_window(void);
static void destroy_context(void);
static void destroy_gwindow(void);
static void unload_context(void);
static void unmap_window(void);

int sys_init(const sys_config_t *config)
{
	const int attrs_glx[] = {
		GLX_X_RENDERABLE,	True,
		GLX_DRAWABLE_TYPE,	GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,	GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,	GLX_TRUE_COLOR,
		GLX_RED_SIZE,		8,
		GLX_GREEN_SIZE,		8,
		GLX_BLUE_SIZE,		8,
		GLX_ALPHA_SIZE,		8,
		GLX_DEPTH_SIZE,		24,
		GLX_STENCIL_SIZE,	8,
		GLX_DOUBLEBUFFER,	True,
		None
	};
	XSetWindowAttributes attrs_x11 = { 0 };
	int nitems = 0;
	XVisualInfo *vis;
	GLXFBConfig *configs;

	if ((dpy = XOpenDisplay(NULL)) == NULL)
		return -1;
	else
		atexit(close_display);

	if ((configs = glXChooseFBConfig(dpy, XDefaultScreen(dpy), attrs_glx, &nitems)) == NULL || nitems == 0)
		return -1;

	if ((vis = glXGetVisualFromFBConfig(dpy, *configs)) == NULL)
		return -1;

	attrs_x11.event_mask = StructureNotifyMask;

	win = XCreateWindow(
		dpy,
		XDefaultRootWindow(dpy),
		config->posx, config->posy,
		config->width, config->height,
		0,
		vis->depth,
		CopyFromParent,
		vis->visual,
		CWEventMask,
		&attrs_x11
	);
	atexit(destroy_window);

	if ((ctx = glXCreateContext(dpy, vis, NULL, True)) == NULL)
		return -1;
	else
		atexit(destroy_context);

	gwin = glXCreateWindow(dpy, *configs, win, NULL);
	atexit(destroy_gwindow);
	
	if ((glXMakeContextCurrent(dpy, gwin, gwin, ctx)) == False)
		return -1;
	else
		atexit(unload_context);

	XFree(vis);
	XFree(configs);

	XMapWindow(dpy, win);
	atexit(unmap_window);

	if ((wm_delete_window = XInternAtom(dpy, "WM_DELETE_WINDOW", False)) == None)
		return -1;

	XSetWMProtocols(dpy, win, &wm_delete_window, 1);

	return 0;
}

void sys_sync(void)
{
	XEvent event;

	while (XPending(dpy)) {
		XNextEvent(dpy, &event);
		event_proc(&event);
	}

	glXSwapBuffers(dpy, gwin);
}

static void close_display(void)
{
	if (dpy != NULL)
		XCloseDisplay(dpy);
}

static void destroy_window(void)
{
	if (dpy != NULL)
		XDestroyWindow(dpy, win);
}

static void destroy_context(void)
{
	if (dpy != NULL && ctx != NULL)
		glXDestroyContext(dpy, ctx);
}

static void destroy_gwindow(void)
{
	if (dpy != NULL)
		glXDestroyWindow(dpy, gwin);
}

static void unload_context(void)
{
	if (dpy != NULL)
		glXMakeContextCurrent(dpy, None, None, NULL);
}

static void unmap_window(void)
{
	if (dpy != NULL)
		XUnmapWindow(dpy, win);
}

static void event_proc(const XEvent *event)
{
	switch (event->type) {
	case ClientMessage:
		if ((Atom) event->xclient.data.l[0] == wm_delete_window)
			exit(EXIT_SUCCESS);

		break;
	default:
		break;
	}
}
